const rp                = require('request-promise');
const $                 = require('cheerio');
const xpath             = require('xpath');
const dom               = require('xmldom').DOMParser;
const url               = require('url');
const extractNumbers    = require('extract-numbers');

const DOMPARSER_CONFIG = {
    locator: {},
    errorHandler: { warning: function (w) { }, 
    error: function (e) { }, 
    fatalError: function (e) { console.error(e) } }
}

const domain = "https://www.danmurphys.com.au/"

const URLS_TO_SCRAPE = [
    /*"https://traibe-crawler.s3.amazonaws.com/archive/sources/5ecd22165dc5c40597e688c0/5ecd224e5dc5c40597e688c1_20200526_140606-01.html",
    "https://traibe-crawler.s3.amazonaws.com/archive/sources/5ecd22165dc5c40597e688c0/5ecd22755dc5c40597e688c2_20200526_140645-41.html",
    "https://traibe-crawler.s3.amazonaws.com/archive/sources/5ecd22165dc5c40597e688c0/5ed76145e0114f058b00dbf0_20200603_083725-09.html",
    "https://traibe-crawler.s3.amazonaws.com/archive/sources/5ecfd3f16a63b4058bee8a61/5edccc9c3e5a566d771bd68f_20200607_111644-01.html",
    "https://traibe-crawler.s3.amazonaws.com/archive/sources/5ecfd3f76a63b4058bee8a81/5edccce43e5a566d771bd692_20200607_111756-33.html",
    "https://traibe-crawler.s3.amazonaws.com/archive/sources/5ecfd3e16a63b4058bee8a09/5edccf273e5a566d771bd69e_20200607_112735-06.html",
    "https://traibe-crawler.s3.amazonaws.com/archive/sources/5ecfd3da6a63b4058bee89e3/5edd3bd43e5a566d771bd98d_20200607_191116-31.html",
    "https://traibe-crawler.s3.amazonaws.com/archive/sources/5ecfd3e06a63b4058bee8a06/5edd41663e5a566d771bd9b7_20200607_193501-96.html",
    "https://traibe-crawler.s3.amazonaws.com/archive/sources/5ecfd3e66a63b4058bee8a25/5edd43673e5a566d771bd9c7_20200607_194335-27.html",
    "https://traibe-crawler.s3.ap-southeast-2.amazonaws.com/sources/394cabef-d7f5-5001-8338-3440322d1acb.html"*/
    "https://traibe-crawler.s3.amazonaws.com/archive/sources/5ecfd4116a63b4058bee8b0b/5ee1736a414660059d40f031_20200610_235730-09.html"
];

const filterValidUrl    = x => x && (x.startsWith('/') || x.startsWith('http'));
const toFullUrl         = x => x.startsWith('/') ? (domain && url.resolve(domain,x)) : x;
const toLowerCase       = x => x.toString().toLowerCase();
const trim              = x => x.toString().trim();
const isNumber          = value => {
    // First: Check typeof and make sure it returns number
    // This code coerces neither booleans nor strings to numbers,
    // although it would be possible to do so if desired.
    if (typeof value !== 'number') {
      return false
    }
    // Reference for typeof:
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof
    // Second: Check for NaN, as NaN is a number to typeof.
    // NaN is the only JavaScript value that never equals itself.
    if (value !== Number(value)) {
      return false
    }
    // Reference for NaN: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/isNaN
    // Note isNaN() is a broken function, but checking for self-equality works as NaN !== NaN
    // Alternatively check for NaN using Number.isNaN(), an ES2015 feature that works how one would expect
  
    // Third: Check for Infinity and -Infinity.
    // Realistically we want finite numbers, or there was probably a division by 0 somewhere.
    if (value === Infinity || value === !Infinity) {
      return false
    }
    return true
}

// Add unique method to array prototype
Array.prototype.scalar  = function(){ return this.length >= 1 ? this[0] : undefined };
Array.prototype.unique  = function(){ return [...new Set(this)]; };
Array.prototype.numeric = function(){
    return this
        .map( a => isNumber(a) ? a : extractNumbers(a))
        .flat()
        .map( b => isNumber(b) ? b : parseFloat(b) );
}

const extractValue = (x) => {
    let cn = x.constructor.name;
    if(cn === 'Text')       return x.nodeValue.trim();
    if(cn === 'Attr')       return x.value;
    if(cn === 'Element')    return x.toString().trim();
}

//
(async () => {

    let promises    = URLS_TO_SCRAPE.map(async (url, index)=>{
        let links           = []
        let price           = undefined
        let brand           = undefined
        let name            = undefined 
        let images          = []
        let rating          = undefined
        let description     = undefined
        let outOfStock      = false
        let variety         = undefined
        try{
            let html    = await rp(url);
            let doc             = new dom(DOMPARSER_CONFIG).parseFromString(html);
            let sel             = s => xpath.select(s, doc).map(extractValue);

            let selLinks        = '//*/a/@href'
            links               = sel(selLinks).filter(filterValidUrl).map(toFullUrl).unique();
            
            let selPrice        = '//*[@id="main-content"]/div/shop-product-detail/div/section/div[1]/aside/shop-add-to-cart/div/div[1]/div/div//text()[contains(.,"per bottle")]';
            price               = sel(selPrice).numeric().scalar();
            
            let selBrand        = '//*[@id="main-content"]/div/shop-product-detail/div/section/h1/span[1]//text()';
            brand               = sel(selBrand).scalar();

            let selName         = '//*[@id="main-content"]/div/shop-product-detail/div/section/h1/span[2]//text()';
            name                = sel(selName).scalar(); 

            let selImg          = '//*/img[@id="xzoom"]/@src'
            images              = sel(selImg).map(toFullUrl).unique();

            let selRating       = '//*[@id="review-panel-v2"]/div/div/shop-customer-reviews/div/div/div/shop-review-snapshot/div/div[2]/div/div[1]/div//text()'
            rating              = sel(selRating).numeric().scalar();

            let selDesc         = '//*[@id="main-content"]/div/shop-product-detail/div/section/div[3]/div[1]/div[2]//text()'
            description         = sel(selDesc).scalar();

            // Out of Stock
            let selOOS          = '//*[@id="main-content"]/div/shop-product-detail/div/section/div[1]/aside/shop-add-to-cart/div/div[2]/div/div/div//text()'
            outOfStock = price === undefined || sel(selOOS).map(toLowerCase).map(trim).includes('out of stock');

            // Variety
            let selVar          = '//*[@id="main-content"]/div/shop-product-detail/div/section/div[3]/div[2]/div/ul/li[1]/span[2]//text()'
            variety             = sel(selVar).map(trim).scalar()
        }catch(err){
            console.log(err);
        }
        return { price, brand, name, variety, description, images, inStock:!outOfStock, rating, links};
    })
    let results  = await Promise.all(promises); 
    console.log(results)
})();
